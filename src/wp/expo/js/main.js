$(window).load(function() {





});





$(document).ready( function() {

		// ViewContent
		// Track key page views (ex: product page, landing page or article)
		fbq('track', 'ViewContent');


	  $('#infoSlider').slick({
	    infinite: true,
	    slidesToShow: 1,
	    slidesToScroll: 1,
	    dots: false,
	    arrows: false,
		  adaptiveHeight: true,
		  autoplaySpeed: 2000,
		  speed: 600
	  });

		$('#infoSlider').on('afterChange', function(event, slick, currentSlide){

			var menuLi= "#"+$(slick.$slides.get(currentSlide)).attr('data-menuId');
	    $("#slickMenu").find("li").removeClass("current");
			$(menuLi).parent().addClass("current");
		});


		$('#slickFuture').click(function (e) {
	    var $this = $(this);

			var mittNummer = 0;
			$('#infoSlider').slick('slickGoTo', mittNummer);
      $this.parent().parent().find("li").removeClass("current");
      $this.parent().addClass("current");


			e.preventDefault();

		});

		$('#slickInovative').click(function(e) {
	    var $this = $(this);

			var mittNummer =1;
			$('#infoSlider').slick('slickGoTo', mittNummer);
      $this.parent().parent().find("li").removeClass("current");
      $this.parent().addClass("current");

			e.preventDefault();

		});

		$('#slickHuman').click(function(e) {
	    var $this = $(this);

			var mittNummer = 2;
			$('#infoSlider').slick('slickGoTo', mittNummer);
      $this.parent().parent().find("li").removeClass("current");
      $this.parent().addClass("current");

			e.preventDefault();

		});

		function imageresize() {

		 var mq = window.matchMedia( "(min-width: 768px)" );

		 if (mq.matches) {
			$('.intro').attr('data-image-src','content/introImage.jpg');
		 } else {
 			$('.intro').attr('data-image-src','content/introImage_mobile.jpg');
		 }
		}


		 imageresize();//Activates when document first loads

		 $(window).bind("resize", function(){
		 imageresize();
		 });

	$("#burger-menu-mobile").click(function () {
        if ($('.small-navigation').css('display') == 'none') $('.small-navigation').show();

        if ($('.navbar').css('display') == 'none')
            $('.navbar').slideDown();
        else
            $('.navbar').slideUp();
    });

});
