<?php

/**
 * Front to the WordPress application. This file doesn't do anything, but loads
 * wp-blog-header.php which does and tells WordPress to load the theme.
 *
 * @package WordPress
 */

/**
 * Tells WordPress to load the WordPress theme and output it.
 *
 * @var bool
 */
define('WP_USE_THEMES', true);

if (function_exists("xdebug_disable")) {
	xdebug_disable();
}

/** Loads the WordPress Environment and Template */

$file = dirname( __FILE__ ) . '/core/wp-blog-header.php';
if (is_file($file)) {
	require $file;
} else {
	echo "No wordpress installed inside ./core";
}