<?php

/*
Template Name: StartPage
*/

get_header();

?>

<div class="container container-full">
    <div class="page-hero start" style="background: url('<?php the_field('hero-background'); ?>');background-repeat: no-repeat;background-position: center center;background-size: cover;webkit-background-size: cover;">
        <p class="start-logo"><img src="<?php echo get_template_directory_uri(); ?>/img/hero-logo-sample.png" alt="" border="0"></p>
        <p class="start-slogan"><?php the_field('hero-text'); ?></p>
        <p class="arrowdown"><a href="#content-section" title="" class="scrollto"><img src="<?php echo get_template_directory_uri(); ?>/img/arrow-down-sample.png" border="0" class="hero-down"></a></p>

        <!-- hero images -->
        <?php
        $heroImages = get_field('hero-images');

        if($heroImages)
        {
            echo '<div class="hero-images">';
            $imageCount = count($heroImages);
            foreach($heroImages as $i)
            {
                echo '<img src="'.$i['url'].'" width="'.(100 / $imageCount).'%" title="" alt="" class="hero-image">';
            }
            echo '</div>';
        }
        ?>

    </div>
</div>

<div class="container container-full">
    <div id="content-section" class="start-block-wrapper brown">
        <div class="content-block center">
            <h1><?php the_field('huvudrubrik'); ?></h1>
            <p><?php the_field('huvudingress'); ?></p>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-4">
            <a href="<?php the_field('vanster_lank'); ?>" class="start-block green">
                <h3><?php the_field('vanster_rubrik'); ?></h3>
                <p><?php the_field('vanster_ingress'); ?></p>
            </a>
        </div>
        <div class="col-md-4">
            <a href="<?php the_field('mitten_lank'); ?>" class="start-block grass">
                <h3><?php the_field('mitten_rubrik'); ?></h3>
                <p><?php the_field('mitten_ingress'); ?></p>
            </a>
        </div>
        <div class="col-md-4">
            <a href="<?php the_field('hoger_lank'); ?>" class="start-block blue">
                <h3><?php the_field('hoger_rubrik'); ?></h3>
                <p><?php the_field('hoger_ingress'); ?></p>
            </a>
        </div>
    </div>
</div>




<div class="container" style="margin-bottom: 60px;">
    <div class="row">

    <hr style="margin-bottom: 40px;"/>

    <div class="col-md-6">

      <div class="puff big <?php the_field('stor_puff_farg'); ?>">
        <a href="<?php the_field('stor_puff_lank'); ?>">
          <img src="<?php the_field('stor_puff_bild'); ?>"/>
          <p><?php the_field('stor_puff_titel'); ?></p>
        </a>
      </div>

          <div class="row">
      <div class="col-md-6">
      <div class="puff small <?php the_field('liten_puff_1_farg'); ?>">
        <a href="<?php the_field('liten_puff_1_lank'); ?>">
          <img src="<?php the_field('liten_puff_1_bild'); ?>"/>
          <p><?php the_field('liten_puff_1_titel'); ?></p>
        </a>
      </div>
      </div>

      <div class="col-md-6">
      <div class="puff small <?php the_field('liten_puff_2_farg'); ?>">
        <a href="<?php the_field('liten_puff_2_lank'); ?>">
          <img src="<?php the_field('liten_puff_2_bild'); ?>"/>
          <p><?php the_field('liten_puff_2_titel'); ?></p>
        </a>
      </div>
      </div>
          </div>


    </div>



    <div class="col-md-6">

      <h3 class="col-header">Aktuellt</h3>

      <?php
      wp_reset_query();
       query_posts('posts_per_page=3');

       if (have_posts()) {
         while (have_posts()) {
          the_post();
        ?>

            <div class="news-list">
            <div class="news-list-image">

              <a href="<?php the_permalink(); ?>" rel="bookmark">

              <?php
                  $src = wp_get_attachment_image_src( get_field('artikelbild') )[0];
                  if(!empty($src))
                      echo '<img src="'.$src.'"/>';
                  else
                      echo '<img src="'. get_template_directory_uri() .'/img/standard_nyhet.jpg" alt="" title="">';
              ?>
              </a>
                  </div>
            <div>
                <span class="news-list-date"><?php the_time('d F Y'); ?></span>
                <a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
                <?php
                if(!empty(get_field('ingress')))
                    echo '<p>' . get_field('ingress') . '</p>';
                else
                    the_excerpt();
                ?>
            </div>
            </div>
        <?php
        }
        }
      ?>



    </div>
    </div>
</div>

<?php get_footer(); ?>
