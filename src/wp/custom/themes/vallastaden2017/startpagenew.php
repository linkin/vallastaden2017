<?php

/*
Template Name: StartPage - ny
*/

get_header();

?>
<div id="startSlider">

  <?php if( have_rows('startslider') ): ?>
    <?php while( have_rows('startslider') ): the_row();

      $image = get_sub_field('bakgrundsbild');
      $rubrik = get_sub_field('rubrik');
      $overlay = get_sub_field('overlayfarg');
      $text = get_sub_field('text');
      $url = get_sub_field('sidlank');
      $cssClass = "";
      if($counterLeft==0){
        $cssClass = 'currentLeft';
      }
      ?>

          <?php if ($url) : ?>
              <a href="<?php echo $url ?>" class="start-jumbotrone" style="background-image: url(<?php echo $image; ?>);">
          <?php else :?>
              <div class="start-jumbotrone" style="background-image: url(<?php echo $image; ?>);">
          <?php endif;?>
            <div class="overlay <?php echo $overlay; ?>"></div>
            <div class="inner">
              <h1><?php echo $rubrik; ?></h1>
              <p><?php echo $text; ?></p>
            </div>
        <?php if ($url) : ?>
            </a>
        <?php else :?>
            </div>
        <?php endif;?>


        <?php $counterLeft++; ?>
    <?php endwhile; ?>
  <?php endif; ?>

</div>

<div class="start-block-group">

    <div class="half-width-box">
        <div class="hero-start-block right-block <?php if( get_field('text_block_hoger') ) echo 'text-block'; ?>" <?php if(get_field('Bakgrundsbild_hoger')) : ?> style="background-image: url(<?php the_field('Bakgrundsbild_hoger'); ?>);" <?php endif; ?>>
            <a href="<?php the_field('lank_block_hoger'); ?>">
                <?php if( get_field('text_block_hoger') ): ?>
                    <div class="text-block-footer">
                        <?php the_field('text_block_hoger'); ?>
                    </div>
                <?php endif ?>
            </a>
        </div>
    </div>

    <div class="half-width-box">
      <div class="hero-start-block left-block <?php if( get_field('text_block_vanster') ) echo 'text-block'; ?>" style="background-image: url(<?php the_field('Bakgrundsbild_vanster'); ?>);">
        <a href="<?php the_field('lank_block_vanster'); ?>">
            <?php if( get_field('text_block_vanster') ): ?>
                <div class="text-block-footer">
                    <?php the_field('text_block_vanster'); ?>
                </div>
            <?php endif ?>
        </a>
      </div>
    </div>

</div>

<div style="clear:both"></div>


<div class="news-container">

    <div class="news-container-inner">

        <div class="filters" >
            <ul id="formats">
                <li class="filter alla current"><a href="#" data-option="*"><i class="fa fa-newspaper-o"></i>Visa nyheter</a></li>
                <li class="filter"><a href="#" data-option="video"><i class="fa fa-play"></i>Visa filmer</a></li>
            </ul>
            <ul id="filters">
                <li class="filter alla current"><a href="#" data-option="*">Alla</a></li>
                <?php
                $categories = get_categories();
                foreach ($categories as $category) {
                  if ($category->term_id > 1) {
                    $option = '<li class="filter '.$category->category_nicename.'">';
                    $option .= '<a href="#" data-option="'.$category->category_nicename.'">';
                    $option .= $category->name;
                    $option .= '</a></li>';
                    echo $option;
                  }
                }
                ?>
            </ul>

        </div>
      <div id="start-new-item-container" class="grid">

      </div>
        <script>
            var vallastaden_newsitems =
                <?php
                        $args = array( 'posts_per_page' => -1);

        $myposts = get_posts( $args );
                $objects = [];
                foreach ($myposts as $post) :
                setup_postdata( $post );
                    $cats = get_the_category();
                    $cat_url = $cats[0]->category_nicename;
                    $cat_name = $cats[0]->name;

                    $obj = (object) [];
                    $obj->sizeLarge = get_field('stor_box_pa_startsidan');

                    if ($cats[0] !== null) {
                        $obj->category_name = $cat_name;
                        $obj->category = $cat_url;
                    }

                    if (get_post_format() === 'video') {
                        $obj->url = get_field('url');
                        $thumbnail = get_field('thumbnailimage');
                        $obj->thumbnail = $thumbnail['sizes']['medium'];
                    }

                    $obj->format = get_post_format();
                    $obj->permalink = get_the_permalink();
                    $obj->title = get_the_title();
                    $obj->excerpt = get_field('ingress'); //get_the_excerpt();
                    $obj->date = get_the_date();

                    if(get_field('startbild')){
                        $image = get_field('startbild');
                        $obj->image = $image['sizes']['medium'];
                    }

                    $objects[] = $obj;

                endforeach;
                echo json_encode($objects);
                wp_reset_postdata();?>
            ;
        </script>
        <button id="start-new-item-loadmore">Läs in fler</button>
    </div>


</div>

<?php get_footer(); ?>
