<?php

/*
Template Name: Objektsida
*/

get_header();

?>

<?php while ( have_posts() ) : the_post(); ?>

    <div class="container container-full">
        <div class="object-hero blue">
            <a href="javascript:window.history.back();" title="Tillbaka" class="object-back"><img src="<?php echo get_template_directory_uri(); ?>/img/ico_back.png" border="0" alt=""> Tillbaka</a>
            <h1><?php the_field('huvudrubrik'); ?></h1>
            <p>
                <span>Bostadstyp: <?php the_field('bostadstyp'); ?></span> |
                <span>Inflyttning: <?php the_field('inflyttning'); ?></span> |
                <span>Antal: <?php the_field('antal'); ?></span>
            </p>
        </div>
    </div>

<div class="container container-small">
    <div class="row">
        <div class="col-md-12">

	        <?php $heroImages = get_field('hero-images'); ?>
	        <div class="obj-caro <?php if (count($heroImages) > 1) : ?>object-carousel<?php endif; ?>">
		        <?php
		        if($heroImages)
		        {
			        foreach ($heroImages as $hImage) {
				        ?>
					        <div>
				                <img alt="" src="<?php echo $hImage['sizes']['large']; ?>" >
						        <?php if (strlen($hImage['caption']) > 0) : ?><h3><?php echo $hImage['caption'] ?></h3><?php endif; ?>
					        </div>
			        <?php
			        }
		        }

		        ?>
	        </div>
        </div>
    </div>

    <div class="row">
        <?php
        // use full width if only one column has content
        if(get_field('text_hoger')) $columnClass = 'col-md-6'; else $columnClass = 'col-md-12';
        ?>
        <div class="<?=$columnClass?>"><?php the_field('text_vanster'); ?></div>
        <?php if(get_field('text_hoger')) echo '<div class="col-md-6">'.get_field('text_hoger').'</div>'; ?>
    </div>
	<?php if (get_field('intresseanmalan')) : ?>
		<div class="row">
			<div class="col-md-12 object-cta-block">
				<hr/>
				<h3>Intresserad av att bo här?</h3>
				<?php if (get_field('lank')) : ?>
				<p>
					För att anmäla intresse för en av Vallastadens bostäder kontaktar du respektive byggherre. Klicka på knappen för att få mer information om byggprojektet.
				</p>
				<button onclick="window.open('<?php the_field('lank'); ?>')">Läs mer här</button>
					<?php else: ?>

						<p><?php the_field('intresseanmalan_text'); ?></p>

				<?php endif; ?>
			</div>
		</div>
	<?php endif; ?>

</div>

<?php endwhile; ?>

<script>
    $('.object-carousel').slick({
	    variableWidth: true,
	    centerMode: true,
	    adaptiveHeight: true
    });
</script>


<?php get_footer(); ?>
