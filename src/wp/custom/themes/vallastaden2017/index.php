<?php

get_header();

?>

    <div class="container container-full news-item">
        <div class="page-hero">
            <h1>Aktuellt</h1>

            <div class="stickyPostWrapper">
                <?php
                    $posts = get_posts(array(
                    'posts_per_page'	=> -1,
                    'post_type'			=> 'post'
                    ,'meta_key' => 'sticky',
                    'meta_value' => true
                    ));
                ?>

                <?php if ( $posts ) : ?>
                    <?php foreach( $posts as $post ):
                        setup_postdata( $post );
                            ?>
                        <div class="stickyPost">
                            <a href="<?php the_permalink(); ?>" rel="bookmark">
                            <?php
                            if(get_field('stickybild'))
                                echo wp_get_attachment_image( get_field('stickybild'), 'stickythumb');
                            else
                                echo '<img src="'. get_template_directory_uri() .'/img/standard_nyhet_sticky.jpg" alt="" title="">';
                                ?>
                            </a>
                            <div>
                                <span class="news-list-date"><?php the_time('d F Y'); ?></span>
                                <a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
                                <?php
                                if(!empty(get_field('ingress')))
                                    echo '<p>' . get_field('ingress') . '</p>';
                                else
                                    the_excerpt();
                                ?>
                            </div>
                        </div>
                        <?php

                    endforeach; ?>

                <?php
                  wp_reset_postdata();
                endif; ?>

            </div>


        </div>
    </div>

    <div class="container news-list-wrapper">

        <div class="row">

            <h4>Alla nyheter från Vallastaden 2017</h4>
            <?php
                $counter=0;
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            $args = array( 'posts_per_page' => 10,
                'paged' => $paged,'post_type' => 'post' );
            $postslist = new WP_Query( $args );
            ?>
            <?php if ( $postslist->have_posts() ) : ?>
                <?php
                    while ( $postslist->have_posts() ) : $postslist->the_post();
        ?>
                    <div class="col-md-6 news-list">
                        <div class="news-list-image"><a href="<?php the_permalink(); ?>" rel="bookmark">
                                <?php
                                if(!empty(get_field('artikelbild')))
                                    echo wp_get_attachment_image( get_field('artikelbild'));
                                else
                                    echo '<img src="'. get_template_directory_uri() .'/img/standard_nyhet.jpg" alt="" title="">';
                                ?></a>
                        </div>
                        <div>
                            <span class="news-list-date"><?php the_time('d F Y'); ?></span>
                            <a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
                            <?php
                            if(!empty(get_field('ingress')))
                                echo '<p>' . get_field('ingress') . '</p>';
                            else
                                the_excerpt();
                            ?>
                        </div>
                    </div>

                    <?php
                    if($counter==1){
                        $counter=0;
                        echo '<div class="row newsListDivider"><div class="col-md-12"><div style="clear:both"><hr/></div></div></div>';
                    }else{
                        $counter = 1;
                    }
                endwhile; ?>

            <?php else : ?>
                <div class="col-md-12">
                    <?php get_template_part( 'content', 'none' ); ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="row">
            <div class="col-md-6 news-navigation">
                <?php next_posts_link('Äldre nyheter', $postslist->max_num_pages); ?>
            </div>
            <div class="col-md-6 news-navigation right">
                <?php previous_posts_link('Nyare nyheter'); ?>
            </div>
        </div>

    </div>

<?php
get_footer();
?>