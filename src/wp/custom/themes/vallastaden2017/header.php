<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title( '|', true, 'right' ) . bloginfo( 'name' ); ?></title>
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/bs/bootstrap.min.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
    <![endif]-->

    <link href="<?php echo get_template_directory_uri(); ?>/css/bs/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/style.css" rel="stylesheet">
    <script src="<?php echo get_template_directory_uri(); ?>/js/slick.min.js"></script>
    <link href="<?php echo get_template_directory_uri(); ?>/js/slick.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/js/slick-theme.css" rel="stylesheet">
    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.ico" type="image/x-icon">
    <script src="<?php echo get_template_directory_uri(); ?>/js/isotope.pkgd.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/imagesloaded.pkgd.min.js"></script>

    <?php
        wp_head();
    ?>
<!--    <script src="//use.typekit.net/oog8jlr.js"></script>-->
<!--    <script>try{Typekit.load();}catch(e){}</script>-->

    <script src="//use.typekit.net/tyi5ytm.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','https://connect.facebook.net/en_US/fbevents.js');

        fbq('init', '183071572098534');
        fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=183071572098534&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->

</head>

<body <?php body_class(); ?>>


<div class="container-head-wrapper">
<div class="container container-head">
    <!--<div class="burger-menu" id="burger-menu"></div>-->
    <div class="burger-menu-mobile" id="burger-menu-mobile"></div>
    <div class="header-logo" onclick="window.location='/'"></div>
    <div class="header-logo-mobile" onclick="window.location='/'"></div>
    <header id="masthead" role="banner">
        <div id="navbar" class="navbar <?php //if(get_field('show_menu')) echo 'dropped'; ?>">

            <nav id="main-navigation" class="main-navigation" role="navigation">
                <?php wp_nav_menu( array( 'theme_location' => 'main-nav', 'menu_class' => 'main-nav' ) ); ?>
            </nav>

            <div id="language-navigation" class="language-navigation dropped">
                <?php wp_nav_menu( array( 'theme_location' => 'language-nav', 'menu_class' => 'language-nav' ) ); ?>
            </div>

        </div><!-- #navbar -->
    </header><!-- #masthead -->
</div>
</div>

    <div id="main" class="site-main">
