<?php

/*
Template Name: Objektöversikt
 */

function encodeString($string)
{
	$string = trim($string);
	$string = strtolower($string);
	$string = str_replace(array('å', 'ä', 'ö', ' '), array('a', 'a', 'o', '-'), $string);
	$string = preg_replace("([^a-z0-9-])", "", $string);
	$string = preg_replace("([-]+)", "-", $string);

	return $string;
}

get_header();

$contentList = get_field('contentblock');
?>

<div class="container container-full">
	<div class="page-hero moreBottomPadding <?php the_field('hero-background'); ?>">
		<h1><?php the_field('hero-large'); ?></h1>

		<h2><?php the_field('hero-small'); ?></h2>

		<p><?php the_field('hero-text'); ?></p>
		<!-- <p><a href="#content-section" title="" class="scrollto"><img src="<?php echo get_template_directory_uri(); ?>/img/arrow-down-sample.png" border="0" class="hero-down"></a></p> -->
	</div>
</div>

<section class="content-section" id="content-section">

	<div class="container object-list-wrapper">

		<div class="moveMap">

			<div id="aboutMap">
				<a href="#" class="cross"><img
						src="<?php echo get_template_directory_uri(); ?>/img/object-back-button-sample.png"/></a>
				<a href="#" class="regularButton"><?php the_field('text_pa_knapp'); ?></a>

				<div>
					<img src="<?php echo get_template_directory_uri(); ?>/img/161004_karta_vallastaden.jpg"/>
				</div>


				<?php
				$contentList = get_field('objects');
				if ($contentList): ?>
					<?php while (has_sub_field('objects')): ?>

						<?php
						$cssClass = get_sub_field('bostadstyp');
						$cssClass = strtolower($cssClass[0]);
						$search   = array('å', 'ä', 'ö');
						$replace  = array('a', 'a', 'o');
						$cssClass = str_replace($search, $replace, $cssClass);
						$cssClass = "villa";

						$lank        = get_sub_field('lank');
						$inflyttning = get_sub_field('inflyttning');
						$rubrik      = get_sub_field('rubrik');
						$boxRubrik   = get_sub_field('rubrik');
						$typ         = join(", ", get_sub_field('bostadstyp'));
						$antal       = get_sub_field('antal');

						?>
						<?php if (have_rows("points")) : ?>
							<?php while (have_rows("points")) : the_row(); ?>
								<?php

								$pointx = get_sub_field('point-x');
								$pointx = ($pointx - 8) / 1120;
								$pointx = $pointx * 100;

								$pointy = get_sub_field('point-y');
								$pointy = ($pointy - 8) / 815;
								$pointy = $pointy * 100;

								if (get_sub_field('box_rubrik')) {
									$rubrik = get_sub_field('box_rubrik');
								} else {
									$rubrik = $boxRubrik;
								}

								$pAntal = $antal;
								if (get_sub_field('count')) {
									$pAntal = get_sub_field('count');
								}

								$pDate = $inflyttning;
								if (get_sub_field('date')) {
									$pDate = get_sub_field('date');
								}

								$pType = $typ;
								if (get_sub_field('type')) {
									$pType = get_sub_field('type');
								}

								$ingress = get_sub_field('ingress_text');

								?>

								<a href="#" class="mapPoint villa" data-link="<?php echo $lank; ?>"
								   data-anchor="<?php echo str_replace(' ', '_', $boxRubrik); ?>"
								   data-name="<?php echo $rubrik; ?>" data-ingress="<?php echo $ingress; ?>"
								   data-type="<?php echo $pType; ?>"
								   data-date="<?php echo $pDate; ?>" data-count="<?php echo $pAntal; ?>"
								   style="left:<?php echo $pointx ?>%;top:<?php echo $pointy ?>%"></a>

							<?php endwhile; ?>
						<?php endif; ?>


						<?php
					endwhile;

				endif;
				?>


				<div class="factBox" id="factBox">
					<a href="#" class="cross" id="factBoxCross"></a>

					<h3 id="name"></h3>

					<p><span id="ingress"></span>
						Bostadstyp: <span id="type"></span></br>
						Inflyttning: <span id="date"></span></br>
						Antal: <span id="count"></span> st</p>

					<a href="#" class="smallButton" id="link">Läs mer</a>
				</div>

			</div>
		</div>

		<div class="filterArea">
			<a href="#" class="filterBt villa" id="lagenhet_bt" data-filter="Lägenhet">Lägenheter</a>
			<a href="#" class="filterBt villa" id="radhus_bt" data-filter="Radhus">Radhus</a>
			<a href="#" class="filterBt villa" id="villa_bt" data-filter="Villa">Villa</a>
			<a href="#" class="filterBt villa" id="offent_bt" data-filter="Offentliga">Offentliga byggnader</a>
		</div>

		<?php
		$contentList = get_field('objects');
		if ($contentList): ?>
				<div class="row">
					<?php while (has_sub_field('objects')): ?>
							<div class="col-md-4 object-list-object" data-type="<?php the_sub_field('bostadstyp'); ?>" id="<?php echo str_replace(' ', '_', get_sub_field('rubrik')) ?>">
								<a <?php echo(get_sub_field('extern_lank') ? 'href="' . get_sub_field('extern_lank') . '" target="_blank"' : 'href="' . get_sub_field('lank') . '"'); ?> title="Läs mer"><img src="<?php the_sub_field('objektbild'); ?>" title="" border="0" alt="" class="object-list-image"></a>
								<div class="object-list-content blue">
									<h3><?php the_sub_field('rubrik'); ?></h3>
									<p><?php the_sub_field('ingress'); ?></p>
									<?php if ( ! in_array('Offentliga', get_sub_field('bostadstyp'))) : ?>
										<p>
											<?php if (get_sub_field('bostadstyp') !== '-') : ?>
												<strong>Bostadstyp:</strong> <?php the_sub_field('bostadstyp'); ?><br>
											<?php endif; ?>
											<?php if (get_sub_field('inflyttning') !== '-') : ?>
												<strong>Inflyttning:</strong> <?php the_sub_field('inflyttning'); ?><br>
											<?php endif; ?>
											<?php if (get_sub_field('antal') !== '-') : ?>
												<strong>Antal:</strong> <?php the_sub_field('antal'); ?>
											<?php endif; ?>
										</p>
										<?php
									else:
										?>
										<p><br><br><br></p>
										<?php
									endif; ?>
									<a <?php echo(get_sub_field('extern_lank') ? 'href="' . get_sub_field('extern_lank') . '" target="_blank"' : 'href="' . get_sub_field('lank') . '"'); ?> class="object-list-button" title="Läs mer">Läs mer</a>
								</div>
							</div>
					<?php	endwhile; ?>
				</div>
			<?php endif; ?>
	</div>


	<div class="container container-full page-content">

		<!-- content repeater -->

		<?php if ($contentList): ?>
			<?php while (has_sub_field('contentblock')): ?>

				<?php if (get_sub_field('typ') == 'text') : ?>

					<div id="<?php echo encodeString(get_sub_field('rubrik')); ?>"
					     class="anchorSection content-block-wrapper<?php if (get_sub_field('bakgrund') != 'none') {
						     echo ' ' . get_sub_field('bakgrund');
					     } ?>">
						<div class="content-block">
							<h3><?php the_sub_field('rubrik'); ?></h3>
							<h4><?php the_sub_field('ingress'); ?></h4>

							<div class="row">
								<?php
								// use full width if only one column has content
								if (get_sub_field('text_hoger')) {
									$columnClass = 'col-md-6';
								} else {
									$columnClass = 'col-md-12';
								}
								?>
								<div class="<?= $columnClass ?>"><?php the_sub_field('text_vanster'); ?></div>
								<?php if (get_sub_field('text_hoger')) {
									echo '<div class="col-md-6">' . get_sub_field('text_hoger') . '</div>';
								} ?>


							</div>
						</div>
						<?php if (get_sub_field('bildblock_under') != '') { ?>
							<div class="image-block">
								<img src="<?php the_sub_field('bildblock_under'); ?>" border="0"
								     class="content-block-image">
							</div>
						<?php } ?>
					</div>

					<?php

				endif;
			endwhile;
		endif;
		?>

		<!-- content repeater -->

	</div><!-- end container -->


</section>

<?php get_footer(); ?>
