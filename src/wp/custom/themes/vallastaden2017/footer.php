<?php
/*
 *  footer
 */
?>


<div class="content-block-wrapper bild backgroundImage" style="background-image:url(<?php the_field('extra_footer_bakgrundsbild', 'option'); ?>)">
  <div class="content-block center extraFooter">
    <?php the_field('extra_footer_innehall', 'option'); ?>
  </div>
</div>


<footer id="colophon" class="main-footer" role="contentinfo">

    <div class="container">
        <div class="row">
            <div class="col-md-2 footer-block">
                <img width="130" src="<?php echo get_template_directory_uri(); ?>/img/footer-logo-sample.png" alt="vallastaden2017.se" title="vallastaden2017.se">
            </div>
            <div class="col-md-4 footer-block">
                <?php wp_nav_menu( array( 'theme_location' => 'footer-nav1', 'menu_class' => 'left' ) ); ?>
                <?php wp_nav_menu( array( 'theme_location' => 'footer-nav2', 'menu_class' => '' ) ); ?>
            </div>
            <div class="col-md-3 footer-block social-media">
                <h3>Följ oss</h3>
                <a href="https://www.facebook.com/vallastaden2017" target="blank"><img align="left" width="20px" src="<?php echo get_template_directory_uri(); ?>/img/fbLink.png" alt="" title="vallastaden2017.se">Facebook</a>
	              <a href="http://www.mynewsdesk.com/se/vallastaden2017" target="blank"><img align="left" width="20px" src="<?php echo get_template_directory_uri(); ?>/img/mynewsdesk-button-30x30.png" alt="" title="vallastaden2017.se">MyNewsdesk</a>
                <a href="https://www.youtube.com/channel/UCEqRIA6fDGvNyOoT1gQTxOQ" target="blank"><img align="left" width="20px" src="<?php echo get_template_directory_uri(); ?>/img/youtubeLink.png" alt="" title="vallastaden2017.se">Youtube</a>

            </div>
            <div class="col-md-3 footer-block">
                <img align="right" src="<?php echo get_template_directory_uri(); ?>/img/linkopinglogo.png" alt="" title="vallastaden2017.se">
            </div>
        </div>
    </div>

</footer>

<?php wp_footer(); ?>


<script src="<?php echo get_template_directory_uri(); ?>/js/main.js?2.3"></script>

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-66681180-1', 'auto');
    ga('send', 'pageview');

</script>

</body>
</html>
