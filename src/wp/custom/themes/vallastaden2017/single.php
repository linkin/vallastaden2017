<?php

// displays single news (posts) items)

get_header();

?>

    <div class="container container-full news-item">
        <div class="page-hero page-hero-single">
            <div class="page-hero-single-news">
            <h2><?php echo get_the_date(); ?></h2>
            <h1 class="small"><?php the_title(); ?></h1>
          </div>
        </div>
    </div>

    <div class="container">
        <div class="row news-row">
            <div class="newsColumn">
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
                    echo '<p class="news-ingress">' . get_field('ingress') . '</p>';
                    /*
                     *  Migrerade nyheter från linkopingsbo2017.se har bildreferenser till den domänen.
                     *  Vi skriver därför om ev. referenser till linkopingsbo2017.se i nyheten innan vi visar den
                     */
                echo str_replace('www.linkopingsbo2017.se/wp-content/','www.vallastaden2017.se/custom/', apply_filters('the_content', get_the_content()) );
                    // the_content();
                endwhile;
                endif;
                ?>
            </div>
            <div class="col-md-3">
                <!--
                <div class="author-meta">
                    <?php
                    // echo get_wp_user_avatar(get_the_author_meta('ID'), 'thumbnail'). '<br>';
                    // echo '<span class="author-name">' . get_the_author_meta('display_name') . '</span><br>';
                    // echo get_the_author_meta('description') . '<br>';
                    ?>
                </div>
                -->
            </div>
        </div>
    </div>

    <div class="container">
<div class="row">
    <div class="col-md-12">
        <p class="news-more-news">Fler aktuella nyheter</p>
    </div>
</div>
        <div class="row">
            <?php query_posts( array( 'posts_per_page' => 10, 'post_status' => 'publish', 'post__not_in' => array( get_the_ID() ) ) ); ?>
            <?php if ( have_posts() ) : ?>
                <?php while ( have_posts() ) : the_post(); ?>
                    <div class="col-md-6 news-list">
                        <div class="news-list-image"><a href="<?php the_permalink(); ?>" rel="bookmark">
                                <?php
                                if(!empty(get_field('artikelbild')))
                                    echo wp_get_attachment_image( get_field('artikelbild'));
                                else
                                    echo '<img src="'. get_template_directory_uri() .'/img/standard_nyhet.jpg" alt="" title="">';
                                ?>
                            </a></div>
                        <div>
                            <span class="news-list-date"><?php the_time('d F Y'); ?></span>
                            <a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
                            <?php
                            if(!empty(get_field('ingress')))
                                echo '<p>' . get_field('ingress') . '</p>';
                            else
                                the_excerpt();
                            ?>
                        </div>
                    </div>
                <?php

                    if($counter==1){
                        $counter=0;
                        echo '<div class="row newsListDivider"><div class="col-md-12"><div style="clear:both"><hr/></div></div></div>';
                    }else{
                        $counter = 1;
                    }

                endwhile; ?>

            <?php else : ?>
                <div class="col-md-12">
                    <?php get_template_part( 'content', 'none' ); ?>
                </div>
            <?php endif; ?>
        </div>

    </div>

<?php get_footer(); ?>
