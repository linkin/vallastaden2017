<?php

/*
Template Name: Standard undersida
*/

function encodeString($string)
{
    $string = trim($string);
    $string = strtolower($string);
    $string = str_replace(array('å', 'ä', 'ö', ' '), array('a', 'a', 'o', '-'), $string);
    $string = preg_replace("([^a-z0-9-])", "", $string);
    $string = preg_replace("([-]+)", "-", $string);
    return $string;
}

get_header();


?>

<?php while ( have_posts() ) : the_post(); $contentList = get_field('contentblock'); ?>

<div class="container container-full">
    <div class="page-hero <?php the_field('hero-background'); ?>" <?php if (get_field('hero-background') == "bild") : ?>style="background-image:url(<?php echo get_field('backgrundsbild'); ?>);"<?php endif; ?>>

      <div class="content-block">
        <div class="row">
          <div class="col-md-12">
              <?php if (strlen(get_field('hero-large-image')) > 0) : ?>
                  <img src="<?php the_field('hero-large-image'); ?>" />
                  <?php else: ?>
                  <h1><?php the_field('hero-large'); ?></h1>
              <?php endif; ?>

	          <?php if (get_field('hero-small')) : ?>
            <h2><?php the_field('hero-small'); ?></h2>
	          <?php endif; ?>
            <p><?php the_field('hero-text'); ?></p>
            <p><a href="#content-section" title="" class="scrollto"><img src="<?php echo get_template_directory_uri(); ?>/img/arrow-down-sample.png" border="0" class="hero-down"></a></p>

              </div>
          </div>
      </div>


      <?php
      if(get_field('karta')) : ?>

      <div class="aboutMapOuter">
        <div id="aboutMap">
          <a href="#" class="cross"><img src="<?php echo get_template_directory_uri(); ?>/img/object-back-button-sample.png"/></a>
          <a href="#" class="regularButton"><?php the_field('text_pa_knapp'); ?></a>
          <div>
            <img src="<?php the_field('kartbild'); ?>"/>
          </div>
        </div>
      </div>

        <?php
        endif;
        ?>

        <!-- hero images -->
        <?php
        $heroImages = get_field('hero-images');

        if($heroImages)
        {
            echo '<div class="hero-images">';
            $imageCount = count($heroImages);
            foreach($heroImages as $i)
            {
                echo '<img src="'.$i['url'].'" width="'.(100 / $imageCount).'%" title="" alt="" class="hero-image">';
            }
            echo '</div>';
        }
        ?>

    </div>
</div>

<section class="content-section" id="content-section">

<!-- sticky sub menu -->
	<?php if (get_field("show_anchors")) : ?>
<div class="sticky-sub-menu-holder" id="sticky-sub-menu-holder">
    <div class="container">
        <div class="col-md-12 sticky-sub-menu">
            <ul id="sticky-sub">
                <?php while(has_sub_field('contentblock')): ?>
                    <?php if(get_sub_field('typ') == 'text' || get_sub_field('typ') == 'tidslinje') echo '<li class="" id="menu_' . encodeString(get_sub_field('rubrik')) . '"><a href="#' . encodeString(get_sub_field('rubrik')) . '" class="scrollto">' . get_sub_field('rubrik') . '</a></li>'; ?>
                <?php endwhile; ?>
            </ul>
        </div>
    </div>
</div>
	<?php endif; ?>
<!-- sticky sub menu -->



    <?php if($post->post_content=="") : ?>

    <?php else : ?>

      <div class="container container-full page-content" style="padding:0">
      	<div class="content-block">
      		<div class="row">
      			<div class="col-md-12">
      		      <?php the_content(); ?>
      			</div>
      		</div>
        </div>
      </div>

    <?php endif; ?>



    <?php if (get_field("show_anchors")) : ?>
    <div class="container container-full page-content">

    <?php else: ?>
    <div class="container container-full page-content noMenu">
    <?php endif; ?>

    <!-- content repeater -->




    <?php if($contentList): ?>

    <?php

          $lastBg = "";

    ?>


        <?php while(has_sub_field('contentblock')): ?>

            <?php if(get_sub_field('typ') == 'text') : ?>

            <?php

              if($lastBg == "empty" && get_sub_field('bakgrund') == 'none' && get_sub_field('show_divider')){
                echo '<hr class="divider"/>';
              }

              if(get_sub_field('bakgrund') == 'none'){
                $lastBg = "empty";
              }else{
                $lastBg = get_sub_field('bakgrund');
              }

            ?>



            <div id="<?php echo encodeString(get_sub_field('rubrik')); ?>" class="anchorSection content-block-wrapper<?php if(get_sub_field('bakgrund') != 'none') echo ' '.get_sub_field('bakgrund'); ?>
              <?php if(get_sub_field('noTopMargin') != '') echo 'noTopMargin'; ?>
              <?php if(get_sub_field('bakgrund') == 'bild') echo ' backgroundImage'; ?> "
              <?php if(get_sub_field('bakgrund') == 'bild') echo ' style="background-image:url(' . get_sub_field('bakgrundsbild') . ' )" '; ?>
                >
                <div class="content-block">


      	          <?php if (get_sub_field('rubrik')) : ?>
                  <h3><?php the_sub_field('rubrik'); ?></h3>
      	          <?php endif; ?>

      	          <?php if (get_sub_field('ingress')) : ?>
                  <h4><?php the_sub_field('ingress'); ?></h4>
      	          <?php endif; ?>

                    <?php if (get_sub_field('text_fore')) : ?>
                        <?php the_sub_field('text_fore'); ?>
                    <?php endif; ?>

                    <div class="row">
                        <?php
                            // use full width if only one column has content
                            if(get_sub_field('text_hoger')) $columnClass = 'col-md-6'; else $columnClass = 'col-md-10 col-md-offset-1';
                        ?>
                        <div class="<?=$columnClass?>"><?php the_sub_field('text_vanster'); ?></div>
                        <?php if(get_sub_field('text_hoger')) echo '<div class="col-md-6">'.get_sub_field('text_hoger').'</div>'; ?>

                    </div>


                    <?php
                    $thumbs = get_sub_field('objektsamling');

                    if($thumbs) : ?>
                        <div class="row">
                            <?php foreach($thumbs as $i) {
                                $image = $i['bild'];
                                $objectLink = $i['url'];
                                ?>
                                <div class="col-sm-3 object-list-object blue">
                                    <a href="<?php echo $i['url'] ?>">
                                        <img src="<?php echo $image['url'] ?>" class="object-list-image object-list-image-bordered">
                                    </a>
                                    <div class="object-list-content object-list-content-sm blue">
                                        <?php if ($i['title'] != "") : ?>
                                            <h3><?php echo $i['titel'] ?></h3>
                                        <?php endif; ?>
                                        <?php echo $i['text'] ?>
                                        <a href="<?php echo $objectLink ?>" class="object-list-button" title="Läs mer">Läs mer</a>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <?php
                    endif
                    ?>

                </div>
                <?php if(get_sub_field('bildblock_under') != ''){ ?>
                <div class="image-block">
                    <img src="<?php the_sub_field('bildblock_under');  ?>" border="0" class="content-block-image">
                </div>
                <?php } ?>

                <?php

                if (get_sub_field('text_hoger'))
                    $docsColumnClass = 'col-md-12';
                else
                    $docsColumnClass = 'col-md-10 col-md-offset-1';

                $contentList = get_sub_field('dokument');
                if($contentList): ?>
                    <div class="content-block">
                    <div class="row">
                        <div class="<?=$docsColumnClass?>">
                            <?php while(has_sub_field('dokument')): ?>

                                <div class="documentList">
                                    <div class="content">
                                        <h5 <?php if (!get_sub_field('text')) : ?>class="only"<?php endif;?>><?php the_sub_field('name'); ?></h5>
                                        <?php if (get_sub_field('text')) : ?><p><?php the_sub_field('text'); ?></p><?php endif; ?>
                                    </div>
                                    <a href="<?php the_sub_field('file'); ?>" target="_blank" class="downloadButton right">Ladda ner</a>
                                    <div style="clear:both;"></div>
                                </div>

                                <?php
                            endwhile;?>
                        </div>
                    </div>
                    </div>
                <?php endif;?>

            </div>


            <?php

            endif;
            if(get_sub_field('typ') == 'tidslinje') :
            ?>


                <div id="<?php echo encodeString(get_sub_field('rubrik')); ?>" class="content-block-wrapper<?php if(get_sub_field('bakgrund') != 'none') echo ' '.get_sub_field('bakgrund'); ?>">
                    <div class="content-block">
                        <h3><?php the_sub_field('rubrik'); ?></h3>
                        <h4><?php the_sub_field('ingress'); ?></h4>
                    </div>


                <div class="tidslinjeOverview">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/time.png" />
                </div>


                <div class="tidslinje">

                    <?php while(has_sub_field('timeline')): ?>

                    <div class="tidslinjeBox <?php the_sub_field('timeline-colour'); ?>">
	                    <?php if (get_sub_field('timeline-image')) : ?>
                      <img src="<?php the_sub_field('timeline-image'); ?>" />
		                    <?php endif; ?>
                      <div class="content">
                        <h5><?php the_sub_field('timeline-headline'); ?></h5>
                        <p class="timeline-date"><?php the_sub_field('timeline-date'); ?></p>
                        <p><?php the_sub_field('timeline-bodytext'); ?></p>
                      </div>
                    </div>

                    <?php
                    endwhile;
                    ?>

                </div>
            </div>

            <?php
            endif;
        endwhile;
    endif;
    ?>


    <!-- content repeater -->

</div><!-- end container -->



</section>
<?php endwhile; ?>



<?php get_footer(); ?>
