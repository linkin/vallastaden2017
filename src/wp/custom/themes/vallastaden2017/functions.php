<?php


@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );

// manage custom menus

register_nav_menu('small-nav', 'top-navigation');
register_nav_menu('main-nav', 'main-navigation');
register_nav_menu('footer-nav1', 'footer-navigation-left');
register_nav_menu('footer-nav2', 'footer-navigation-right');
register_nav_menu('language-nav', 'language-navigation');

function custom_excerpt_length( $length ) {
    return 15;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

add_image_size('stickythumb', 315, 165, true );
add_image_size('startgridthumb', 742, 454, true );

add_theme_support('post-formats', array('video'));


function custom_embed($html, $url, $attr, $post_id) {
    return '<div class="embed-responsive embed-responsive-16by9">' . $html . '</div>';
}
add_filter('embed_oembed_html', 'custom_embed', 99, 4);

if( function_exists('acf_add_options_page') ) {

    acf_add_options_page();

}


