<?php

/*
 *  custom 404 page for the vallastaden2017 theme
 *
 */

get_header();

?>

    <div class="container container-full news-item">
        <div class="page-hero">
            <h1>404</h1>
        </div>
    </div>

<div class="container">
    <div class="row">
        <div class="col-md-12" style="text-align: center;padding:50px 0">
            <h3>Sidan du letar efter kunde inte hittas</h3>
            <p>
                Prova igen eller kontakta oss om du upplever att sidan saknas helt.
            </p>
        </div>
    </div>
</div>

<?php get_footer(); ?>