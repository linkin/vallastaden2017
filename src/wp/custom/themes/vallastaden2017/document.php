<?php

/*
Template Name: Dokument
*/

get_header();

?>

<div class="container container-full">
    <div class="page-hero <?php the_field('huvud-bakgrund'); ?>">
        <h1><?php the_field('hero-large'); ?></h1>
        <h2><?php the_field('hero-small'); ?></h2>
    </div>
</div>

<div class="container container-small">
    <div class="row">
        <div class="col-md-12">
            <div class="documentWrapper">
                <?php if (get_field('inledning_rubrik') || get_field('inledning_text')): ?>
                    <div class="content-block">
                        <h4><?php the_field('inledning_rubrik') ?></h4>
                        <?php the_field('inledning_text') ?>
                    </div>
                <?php endif; ?>
              <?php
              $contentList = get_field('dokument');
              if($contentList): ?>
              <?php while(has_sub_field('dokument')): ?>

                <div class="documentList">
                <div class="content">
                <h5><?php the_sub_field('name'); ?></h5>
                <p><?php the_sub_field('text'); ?></p>
                </div>
                <a href="<?php the_sub_field('file'); ?>" target="_blank" class="downloadButton right">Ladda ner</a>
                <div style="clear:both;"></div>
                </div>

              <?php
              endwhile;

              endif;
              ?>

            </div>
        </div>
    </div>
</div>


<?php get_footer(); ?>
