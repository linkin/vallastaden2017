var vallastaden_templates = {
    newsItem: function (obj) {

        var largeClass = "";
        if (obj.sizeLarge || obj.format == 'video') {
            //largeClass = "grid-item--width2";
        }

        if (obj.format == 'video') {
            largeClass += " grid-item-video";
        }

        var category = '';
        if (obj.category !== undefined) {
            category = obj.category;
        }

        var html = '';
        html += '<div class="grid-item start-news-item ' + largeClass + '" data-category="' + category + '" data-format="' + obj.format + '">';
        html += '<div class="grid-item-inner">';
        html += '<a href="' + obj.permalink + '">';

        if (obj.image !== undefined) {
            html += '<img src="' + obj.image + '" />';
        }

        if (obj.format == 'video') {
            html += '<div class="embed-responsive embed-responsive-16by9">';
            html += '<img class="news-preview" src="'+obj.thumbnail+'" />';
            html += '<iframe class="news-video" width="500" height="281" src="" data-src="' + obj.url + '" frameborder="0" allowfullscreen=""></iframe>';
            html += '</div>';
        }

        html += '<p class="news-headline">' + obj.title + '</p>';
        html += '<p>' + obj.excerpt + '</p>';
        html += '<p class="tagDate">';

        if (obj.category_name !== undefined) {
            html += '<span class="tag">' + obj.category_name + '</span>';
        }

        html += '<span class="date">' + obj.date + '</span></p>';

        html += '</a>';
        html += '</div>';
        html += '</div>';
        return html;
    }
}


$(document).ready(function () {

    // ViewContent
    // Track key page views (ex: product page, landing page or article)
    fbq('track', 'ViewContent');

    /* main menu toggling */
    if ($(".grid").length) {
        $('.grid').isotope({
            // options
            itemSelector: '.grid-item',
            isResizable: true,
            percentPosition: true
        });

        //setTimeout(function() {
        //  $('.grid').isotope();
        //}, 100);

    }

    $("#filters a").click(function (e) {

        var currentFormat = $('#formats li.current a').attr('data-option');
        var currentCat = $(this).attr("data-option");

        $("#filters a").parent().removeClass("current");
        $(this).parent().addClass("current");

        $('#start-new-item-container').trigger('filter', [currentCat, currentFormat]);

        e.preventDefault();
    });

    $('#formats a').click(function (e) {

        var currentCat = $('#filters li.current a').attr('data-option');
        var currentFormat = $(this).attr("data-option");

        $("#formats a").parent().removeClass("current");
        $(this).parent().addClass("current");

        $('#start-new-item-container').trigger('filter', [currentCat, currentFormat]);

        e.preventDefault();
    });

    $('.right-block a').click(function() {
        $('#formats a[data-option="video"]').trigger('click');
        return false;
    });

    $('#start-new-item-container').on('filter', function(e, category, format) {

        var filterString = '';
        if (category !== '*') {
            filterString += '[data-category="' + category + '"]';
        }

        if (format !== '*') {
            filterString += '[data-format="' + format + '"]';
        }

        $('#start-new-item-container').isotope({filter: filterString});
        $('html,body').animate({scrollTop: $("#filters").offset().top - 75}, 'slow');
    })


    $('#start-new-item-loadmore').click(function () {

        if (!$(this).hasClass('disabled')) {
            var html = "";
            var slice = vallastaden_newsitems.splice(0, 10);

            for (var i in slice) {
                var newsItem = slice[i];
                html += vallastaden_templates.newsItem(newsItem);
            }

            $('.grid').isotope('insert', $(html));
            $('.grid').imagesLoaded().progress(function () {
                $('.grid').isotope('layout');
            });

            if (vallastaden_newsitems.length == 0) {
                $(this).addClass('disabled');
            }
        }


    }).trigger('click');


    $('#start-new-item-container').on('click', '.news-preview', function() {
        $('.grid .grid-item-playing').each(function() {
            $(this).find('.news-video')[0].src = '';
            $(this).removeClass('grid-item-playing');
        });
        $(this).parent().parent().parent().parent().addClass('grid-item-playing');

        var $video = $(this).parent().parent().parent().parent().find('.news-video');
        $video[0].src = $video.attr('data-src')+'?autoplay=1';
        $('.grid').isotope('layout');
        return false;
    });

    $("#burger-menu").click(function () {
        console.log('stora');
        if ($('.navbar').css('display') == 'none') $('.navbar').show();
        if ($('.small-navigation').css('display') == 'none') {
            $('.small-navigation').slideDown({
                'start': function () {
                    $("#navbar").animate({
                        'padding-top': '45px',
                    });
                }
            });
        } else {
            $('.small-navigation').slideUp({
                'start': function () {
                    $("#navbar").animate({
                        'padding-top': 0,
                    });
                }
            });
        }
    });

    $("#burger-menu-mobile").click(function () {
        if ($('.small-navigation').css('display') == 'none') $('.small-navigation').show();

        if ($('.navbar').css('display') == 'none')
            $('.navbar').slideDown();
        else
            $('.navbar').slideUp();
    });

    $(".scrollto").click(function () {
        $("html, body").animate({scrollTop: $($(this).attr("href")).offset().top}, 500);

        return false;
    });

    if ($("#sticky-sub-menu-holder").length == 1) {


        // --- sticky sub menu controls ---
        var windowHolder = $(window);
        var contentSection = $(".content-section");
        var stickySubMenu = $(".sticky-sub-menu-holder");
        var contentSectionTop;

        function setOffsetPositions() {

            var stickySubMenuOffset = contentSection.offset();
            contentSectionTop = stickySubMenuOffset.top - 0;

        }

        function checkstickySubMenu() {

            if (windowHolder.scrollTop() > contentSectionTop) {
                stickySubMenu.addClass('fixed');
                stickySubMenu.removeClass('fixedAfter');
            }
            else {
                stickySubMenu.removeClass('fixed');
                stickySubMenu.removeClass('fixedAfter');
            }

        }

        windowHolder.resize(function () {

            setOffsetPositions();
            checkstickySubMenu();

        }).scroll(function () {

            setOffsetPositions();
            checkstickySubMenu();

        });

        setOffsetPositions();
        checkstickySubMenu();
    }


    var $objects = $(".object-list-object");

    var $points = $(".moveMap .mapPoint");
    var $mapFactBox = $("#factBox");

    $('.filterBt').click(function () {

        $(this).toggleClass('empty');

        var activeFilters = [];

        $(".filterBt").each(function () {
            if (!$(this).hasClass("empty")) {
                activeFilters.push($(this).attr("data-filter"));
            }
        });

        $(".object-list-object, .mapPoint").each(function (index) {
            var types = $(this).attr("data-type").replace(" ", "").split(",");
            var hits = 0;

            for (var i in types) {
                var type = types[i];
                if (activeFilters.indexOf(type) >= 0) {
                    hits++;
                }
            }

            if (hits === 0) {
                $(this).addClass("hidden");
            } else {
                $(this).removeClass("hidden");
            }
        });

        return false;
    });


    $(".mapPoint").click(function () {
        $mapFactBox.addClass("show Villa");

        /*
         $mapFactBox.removeClass("Lagenhet");
         $mapFactBox.removeClass("Radhus");
         $mapFactBox.removeClass("Villa");*/

        $mapFactBox.addClass($(this).data("type").replace('ä', 'a'));


        $("#name").text($(this).data("name"));
        $("#ingress").text($(this).data("ingress"));
        if (!$("#ingress").is(':empty')) $("#ingress").append('<br><br>');
        $("#type").text($(this).data("type"));
        $("#date").text($(this).data("date"));
        $("#count").text($(this).data("count"));

        if ($(this).data("type") == 'Offentliga') {
            $("#link").attr("href", "#" + $(this).data("anchor").replace(/ /g, '_'));
        } else {
            $("#link").attr("href", $(this).data("link"));
        }

        var position = $(this).position();
        var $factBoxTop = position.top - $mapFactBox.outerHeight() - 20 + "px";


        $mapFactBox.css({
            "left": $(this).css("left"),
            "top": $factBoxTop
        });
        return false;
    });
    $("#factBoxCross").click(function () {
        $mapFactBox.removeClass("show");
        return false;
    });


    $("#aboutMap").on('click', '.regularButton, .cross', function (e) {
        $(e.delegateTarget).toggleClass("clicked");
        return false;
    });


    $('#startSlider').slick({
        dots: true,
        infinite: true,
        slidesToShow: 1,
        fade: true,
        slidesToScroll: 1,
        autoplay: true,
        speed: 2500,
        pauseOnHover: false,
        autoplaySpeed: 2000,
        responsive: [
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    arrows: false,
                    dots: true
                }
            }
        ]
    });

    //setTimeout(function() {
    //  $('.grid').isotope();
    //}, 100);


    $('.tidslinje').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 767,
                settings: {
                    arrows: false,
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });

    $('.stickyPostWrapper').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 767,
                settings: {
                    arrows: false,
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });

    var isAnimatin = false;

    var $menu = $("#sticky-sub li");
    var $anchors = $menu.find("a");
    var $anchorSections = $(".anchorSection");

    function lookAfterChapter() {
        if (isAnimatin == false) {

            $anchorSections.each(function () {

                var $section = $(this);
                var id = $section.attr("id");

                if ($section.isOnScreen()) {
                    $menu.removeClass("current");
                    $anchors.filter('[href="#' + id + '"]').parent().addClass("current");
                    return false; //Bryter looped så vi inte tittar vidare
                }

            });


        }
    }

    $(window).scroll(function () {
        lookAfterChapter();
    });

    $.fn.isOnScreen = function () {

        var win = $(window);

        var viewport = {
            top: win.scrollTop() + 200,
            left: win.scrollLeft()
        };
        viewport.right = viewport.left + win.width();
        viewport.bottom = viewport.top + win.height();

        var bounds = this.offset();
        bounds.right = bounds.left + this.outerWidth();
        bounds.bottom = bounds.top + this.outerHeight();

        return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));

    };

});
